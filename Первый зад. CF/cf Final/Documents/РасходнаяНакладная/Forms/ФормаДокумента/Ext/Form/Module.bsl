﻿&НаКлиенте
Процедура ТЧКоличествоПриИзменении(Элемент)
	
	СтрокаТабличнойЧасти = Элементы.Товары.ТекущиеДанные;
	РасчетСуммы.РассчитатьСумму(строкатабличнойчасти)
	
КонецПроцедуры

&НаКлиенте
Процедура ТЧЦенаПриИзменении(Элемент)
	
	СтрокаТабличнойЧасти = Элементы.Товары.ТекущиеДанные;
	РасчетСуммы.РассчитатьСумму(строкатабличнойчасти)
	
КонецПроцедуры

&НаКлиенте
Процедура ТЧНоменклатураПриИзменении(Элемент)
	
	//Получить текущую строку табличной части.
	СтрокаТабличнойЧасти = Элементы.Товары.ТекущиеДанные;
	
	//Установить Цену.
	СтрокаТабличнойЧасти.Цена = РозничнаяЦена(Объект.Дата, СтрокаТабличнойЧасти.Номенклатура) + РозничнаяЦена(Объект.Дата, СтрокаТабличнойЧасти.Номенклатура) * Наценка() / 100 ;
	
	//Пересчитать сумму строки.
	СтрокаТабличнойЧасти.Сумма = СтрокаТабличнойЧасти.Количество * СтрокаТабличнойЧасти.Цена;
	
КонецПроцедуры

&НаСервере
Функция РозничнаяЦена(АктуальнаяДата, ЭлементНоменклатуры) 
	
	Возврат РозничнаяЦенаАктуальная.РозничнаяЦена(АктуальнаяДата, ЭлементНоменклатуры);
	
КонецФункции

&НаСервере
Функция Наценка() 
	
	Возврат УчетНаценки.Наценка();
	
КонецФункции










